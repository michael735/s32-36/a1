const express = require('express');
const router = express.Router();

const {verify,verifyAdmin} = require('./../auth')

const {createCourse,
        getAllCourse,
        findCourse,
        updateCourse,
        archive,
        unArchive,
        findActive,
        deleteCourse} = require('./../controllers/courseControllers')


//Create a route /create to create a new course, save the course in DB and return the document
//Only admin can create a course

router.post('/create', verifyAdmin, async(req,res) => {

    try{
        await createCourse(req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})


//Create a route "/" to get all the courses, return all documents found
    //both admin and regular user can access this route
router.get(`/`, verify, async(req, res) => {
    
    try{
        await getAllCourse().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})



//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
    //both admin and regular user can access this route

router.get(`/:courseId`, verify, async(req, res) => {

    const courseId = req.params.courseId;

    try{
        await findCourse(courseId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})
    

//Create a route "/:courseId/update" to update course info, return the updated document
    //Only admin can update a course

router.patch('/:courseId/update', verifyAdmin, async (req, res) => {

    const courseId = req.params.courseId;
    
    try{
        await updateCourse(courseId, req.body).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})



//Create a route "/:courseId/archive" to update isActive to false, return updated document
    //Only admin can update a course

router.patch('/:courseId/archive', verifyAdmin, async (req, res) => {

    const courseId = req.params.courseId;
    
    try{
        await archive(courseId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})


//Create a route "/:courseId/unArchive" to update isActive to true, return updated document
    //Only admin can update a course

router.patch('/:courseId/unArchive', verifyAdmin, async (req, res) => {

    const courseId = req.params.courseId;
    
    try{
        await unArchive(courseId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})


//Create a route "/isActive" to get all active courses, return all documents found
    //both admin and regular user can access this route

router.get(`/isActive`, verify, async(res) => {

    try{
        await findActive().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})
    

//Create a route "/:id/delete-course" to delete a courses, return true if success
    //Only admin can delete a course

router.delete('/:courseId/delete', verifyAdmin, async (req, res) => {

    const courseId = req.params.courseId;
    try{
        await deleteCourse(courseId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})
    

//Export the router module to be used in index.js file
module.exports = router;
