const express = require('express');
const router = express.Router();

const {verify,decode,verifyAdmin} = require('./../auth')

//Import units of functions from userController module

const{register, 
	getAllUsers, 
	checkEmail, 
	login,
	profile,
	changeInfo,
	changePass,
	changeToAdmin,
	changeToUser,
	deleteUser} = require('./../controllers/userControllers')


router.get(`/`, (req, res) => {
    res.send(`Welcome to get route`)
})

//REGISTER A USER

router.post('/register', async (req, res) =>{
    // console.log(req.body)

    try{
        await register(req.body).then(result => res.send(result))
    }catch(err){
            res.status(500).json(err)
    }
})

//GET ALL USERS
router.get(`/getall`, async(req, res) => {   
    try{
        await getAllUsers().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
} 
})


//Check email

router.post(`/email-exists`, async (req, res) => {
    try{
        await checkEmail(req.body).then(result => res.send(result))
    }catch(error){
        res.status(500).json(error)
  }
})

//LOGIN THE USER

router.post('/login', async(req, res) => {
	//console.log(req.body)
	try{
        await login(req.body).then(result => res.send(result))
    }catch(error){
        res.status(500).json(error)
  }
})


//RETRIEVE USER INFO

router.get('/profile', verify,  (req, res) =>{
	 //res.send(`Welcome to get profile request`)
	 const userId = decode(req.headers.authorization).id

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//Create a route to update user information, make sure the route is secured with token

router.put('/changeinfo', verify, async(req, res) => {
	console

	const userId = decode(req.headers.authorization).id
	try{
		await changeInfo(userId, req.body).then(result=> res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})
//Create a route to update user's password, make sure the  route is secured with token

// router.put('/changepass', verify, async(req, res) => {

// 	const userId = decode(req.headers.authorization).id
// 	try{
// 		await changePass(userId, req.body).then(result=> res.send(result))
// 	}catch(err){
// 		res.status(500).json(err)
// 	}
// })


//USING PATCH

router.patch('/changepass', verify, async(req, res) => {
	const userId = decode(req.headers.authorization).id
	try{
		await changePass(userId, req.body).then(result=> res.send(result))
	}catch{
		res.status(500).json(err)
	}

})

//Create a route /isAdmin to update user's isAdmin status to true, make sure the  route is secured with token
	//Only admin can update user's isAdmin status

// router.patch('/toAdmin', verify, async (req, res) =>{

// 	const isAdmin = decode(req.headers.authorization).isAdmin
// 	try{
// 		if(isAdmin == true){
// 			await changeToAdmin(req.body).then(result => res.send(result))
// 		}else{
// 			res.send("You are not authorized")
// 		}	
// 	}catch(err){
// 		res.status(500).json(err)
// 	}
// })

router.patch('/toAdmin', verifyAdmin, async (req, res) => {
	try{
		await changeToAdmin(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

router.patch('/toUser', verifyAdmin, async (req, res) => {
	try{
		await changeToUser(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

//Create a route to dekete a user from the DB, make sure the route is secured
	// Stretch goal, only admin can delete a user

router.delete('/deleteUser', verifyAdmin, async(req, res)=> {
	try{
		await deleteUser(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//Export the router module to be used in index.js file
module.exports = router;
