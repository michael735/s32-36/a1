const User = require('./../models/User');
const AES = require("crypto-js/aes");
const CryptoJS = require("crypto-js");
const {createToken} = require('./../auth');
const { findOneAndUpdate } = require('./../models/User');


//REGISTER A USER
module.exports.register = async (reqBody) => {
	// console.log(reqBody)
	const {firstName, lastName, email, password} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	})

	return await  newUser.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


//GET ALL USERS
module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}


module.exports.checkEmail = async(reqBody) => {
    const {email} = reqBody
    return await User.findOne({email: email}).then((result, err) => {
        if(result){
            return true
        }else{
            if(result == null){
                return false
            }else {
                return err
            }
        }
    })
}

//LOGIN A USER

module.exports.login = async (reqBody) => {
	return await User.findOne({email: reqBody.email}).then((result, err) =>{
		if (result == null){
			return {message: `User does not exist`}

		}else{ 

			if (result !== null ){
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8);
				if(reqBody.password == decryptedPw){
					//console.log("Token created successfully")
					return {token: createToken(result)}
				}else{
					return{auth: `Auth Failed`}
				}
			}else{
				return err
			}
		}
	})

}

//FIND BY ID
module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})
}

//UPDATE USER INFO

module.exports.changeInfo = async (userId, reqBody) => {


	const  userData = ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName, 
		email: reqBody.email,
		password: CryptoJS.AES.encrypt(reqBody.CryptoJSpassword, process.env.SECRET_PASS).toString()
	})

	return await User.findByIdAndUpdate(userId, {$set: userData}, {new:true}).then((result, err) => {
		if(result){
			result.password = "***"
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})

}

// CHANGE PASS USING PUT
// module.exports.changePass = async (userId, reqBody) => {

// 	const newPass = ({password: CryptoJS.AES.encrypt(reqBody.CryptoJSpassword, process.env.SECRET_PASS).toString()
// 	})

// 	return await User.findByIdAndUpdate(userId, {$set: newPass}, {new:true}).then((result, err) => {
// 		if(result){
// 			result.password ="*********"
// 			return result
// 		}else{
// 			if(result == null){
// 				return {message: `user does not exist`}
// 			} else {
// 				return err
// 			}
// 		}
// 	})

// }


// CHANGE PASS USING PATCH

module.exports.changePass = async(id, reqBody) => {

	const newPass = ({password: CryptoJS.AES.encrypt(reqBody.CryptoJSpassword, process.env.SECRET_PASS).toString()
	})

	return await User.findByIdAndUpdate(id, {$set: newPass}, {new:true}).then((result, err) => {
		if(result){
			result.password ="*********"
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})

}

//CHANGE ISADMIN

module.exports.changeToAdmin = async(reqBody) => {
	const {email} = reqBody

	return await User.findOneAndUpdate({email: email}, {$set:{isAdmin: true}}, {new:true}).then ((result,err) => result ? result : err)
}

//CHANGE TO USER

module.exports.changeToUser = async(reqBody) => {
	const {email} = reqBody

	return await User.findOneAndUpdate({email: email}, {$set:{isAdmin: false}}, {new:true}).then ((result,err) => result ? result: err)
}

//DELETE USER

module.exports.deleteUser = async(reqBody) => {
	const {email} = reqBody
	return await User.findOneAndDelete({email: email}).then ((result,err) => result ? true: err)
}
