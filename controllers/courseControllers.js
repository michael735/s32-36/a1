const Course = require('../models/Course');

// CREATE COURSE

module.exports.createCourse = async (reqBody) => {

	const {courseName, description, price} = reqBody

	const newCourse = new Course({
		courseName: courseName,
		description: description,
		price: price,
	})

	return await newCourse.save(reqBody).then(result =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})

}

//GET ALL COURSES

module.exports.getAllCourse = async () => {

	return await Course.find().then(result => result)
}

//FIND COURSE
module.exports.findCourse = async (id) => {

	return await Course.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Course does not exist`}
			} else {
				return err
			}
		}
	})
}


//UPDATE COURSE

module.exports.updateCourse = async (id, reqBody) => {
	
	const courseData = ({
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price,
	})

	return await Course.findByIdAndUpdate(id, {$set: courseData}, {new:true}).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Course does not exist`}
			} else {
				return err
			}
		}
	})

}

//Archive Course

module.exports.archive= async(id) => {

	return await Course.findByIdAndUpdate(id, {$set:{isOffered: false}}, {new:true}).then ((result,err) => result ? result : err)
}


//Unarchive Course

module.exports.unArchive= async(id) => {

	return await Course.findByIdAndUpdate(id, {$set:{isOffered: true}}, {new:true}).then ((result,err) => result ? result : err)
}

// FIND ACTIVE

module.exports.findActive = async () => {

	return await Course.find({"isOffered": true}).then(result => result)
}

//Delete Course

module.exports.deleteCourse= async(id) => {

	return await Course.findByIdAndDelete(id).then ((result,err) => result ? true : err)
}

